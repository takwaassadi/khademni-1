import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:khademni/log_in.dart';

import 'First_Page.dart';

class PasswordReset extends StatefulWidget {
  const PasswordReset({Key? key}) : super(key: key);

  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  final TextEditingController emailController = new TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(

        reverse: true ,
        child: Column(

          children: [

            Container(
              width: w*0.7,
              height: h*0.3,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          "img/logo.png"
                      ),
                      fit: BoxFit.cover
                  )
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: [
                  const Text("Reset Password",
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.normal,
                          color: Color(0xFF7779E9)
                      )
                  ),
                  SizedBox(height: 20,),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      controller: emailController,
                      validator: (value) {
                        if (value==null ||  value.trim().isEmpty) {
                          return ("Please Enter Your Email");
                        }
                        // reg expression for email validation
                        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                            .hasMatch(value)) {
                          return ("Please Enter a valid email");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        emailController.text = value!;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          hintText: 'Email',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),

                          ),
                          suffixIcon: Icon(Icons.alternate_email)
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),

                  SizedBox(height: 80,),
                  ElevatedButton(onPressed: () async{
                    resetPassword();
                    Fluttertoast.showToast(
                        msg: "Kindly reset your password using the link sent on your mail",
                        gravity: ToastGravity.CENTER,);
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) =>  const LogInPageFinal()));
                  },
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(400, 60),
                        primary: Colors.deepPurpleAccent,
                        onPrimary: Colors.black,
                        shape:  RoundedRectangleBorder(
                          borderRadius:  BorderRadius.circular(30.0),
                        )

                    ),
                    child: const Text(
                        "Reset Password",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        )
                    ),

                  ),
                  SizedBox(height: 10,),
                  ElevatedButton(onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) =>  const LoginPage()));
                  },
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(400, 60),
                        primary: Colors.deepPurpleAccent,
                        onPrimary: Colors.black,
                        shape:  RoundedRectangleBorder(
                          borderRadius:  BorderRadius.circular(30.0),
                        )

                    ),
                    child: const Text(
                        "Annuler",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        )
                    ),

                  ),
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
  void resetPassword() async {

    String email = emailController.text.toString();
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
    }
    catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }
}
