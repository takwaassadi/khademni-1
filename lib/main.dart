import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:khademni/ghaith/MyHomePage.dart';
import 'package:khademni/log_in.dart';
import 'package:khademni/First_Page.dart';
import 'package:khademni/profile.dart';
import 'package:khademni/sign_up.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';



Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await  Firebase.initializeApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:  MyHomePage(),
    );
  }
}

