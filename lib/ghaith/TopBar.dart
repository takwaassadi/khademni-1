// ignore_for_file: file_names, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:khademni/ghaith/SearchBar.dart';

class TopBar extends StatelessWidget {
  const TopBar({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row( 
      children:[
      Expanded(child: SearchBar()),
      SizedBox(
        width: 10,
      ),
      Icon(Icons.menu,
      size:30,)
      ],  
    );
  }
}