class UserModel {
  String? uid;
  String? email;
  String? name;
  String? lastname;

  UserModel({this.uid, this.email, this.name, this.lastname});

  // receiving data from server
  factory UserModel.fromMap(map) {
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      name: map['firstName'],
      lastname: map['secondName'],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'Name': name,
      'LastName': lastname,
    };
  }
}