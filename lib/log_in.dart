import 'package:flutter/material.dart';
import 'package:khademni/First_Page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:khademni/profile.dart';
import 'package:khademni/reset_password.dart';

class LogInPageFinal extends StatefulWidget {
  const LogInPageFinal({Key? key}) : super(key: key);

  @override
  _LogInPageFinalState createState() => _LogInPageFinalState();
}

class _LogInPageFinalState extends State<LogInPageFinal> {
 bool hidepassword=true;
 final _formKey = GlobalKey<FormState>();
 final TextEditingController emailController = new TextEditingController();
 final TextEditingController passwordController = new TextEditingController();

 // firebase
 final _auth = FirebaseAuth.instance;

 // string for displaying the error Message
 String? errorMessage;
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        reverse: true ,
        child: Column(

          children: [

            Container(
              width: w*0.7,
              height: h*0.3,
               decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        "img/logo.png"
                          ),
                      fit: BoxFit.cover
                 )
               ),
              ),
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: [
                  const Text("Log In",
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.normal,
                        color: Color(0xFF7779F1)
                      )
                  ),
                  SizedBox(height: 20,),
                  Container(
                    decoration:BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          offset:Offset(1,1),
                          color: Colors.grey.withOpacity(1)
                        )
                      ]
                    ) ,
                    child: TextFormField(
                      controller: emailController,
                      validator: (value) {
                        if (value==null ||  value.trim().isEmpty) {
                          return ("Please Enter Your Email");
                        }
                        // reg expression for email validation
                        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                            .hasMatch(value)) {
                          return ("Please Enter a valid email");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        emailController.text = value!;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),

                        ),
                          suffixIcon: Icon(Icons.alternate_email)
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    decoration:BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset:Offset(1,1),
                              color: Colors.grey.withOpacity(1)
                          )
                        ]
                    ) ,
                    child: TextFormField(
                      controller: passwordController,

                      validator: (value) {
                        RegExp regex = new RegExp(r'^.{6,}$');
                        if (value==null ||  value.trim().isEmpty ) {
                          return ("Password is required for login");
                        }
                        if (!regex.hasMatch(value)) {
                          return ("Enter Valid Password(Min. 6 Character)");
                        }
                      },
                      onSaved: (value) {
                        passwordController.text = value!;
                      },
                      textInputAction: TextInputAction.done,
                      obscureText: hidepassword,
                      decoration: InputDecoration(
                          suffixIcon: InkWell(
                            onTap: _togglepassword,
                            child: const Icon(
                              Icons.visibility,
                            ),
                          ),
                          hintText: 'Mot de passe',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                      ),
                    ),
                  ),
                  SizedBox(height: 80,),
                  ElevatedButton(onPressed: (){
                    signIn(emailController.text , passwordController.text);
                  },
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(300, 60),
                        primary: Colors.deepPurpleAccent,
                        onPrimary: Colors.black,
                        shape:  RoundedRectangleBorder(
                          borderRadius:  BorderRadius.circular(30.0),
                        )

                    ),
                    child: const Text(
                        "Connecter",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        )
                    ),

                  ),
                  SizedBox(height: 10,),
                  ElevatedButton(onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) =>  const LoginPage()));
                  },
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(200, 60),
                        primary: Colors.deepPurpleAccent,
                        onPrimary: Colors.black,
                        shape:  RoundedRectangleBorder(
                          borderRadius:  BorderRadius.circular(30.0),

                        )

                    ),
                    child: const Text(
                        "Retourner",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        )
                    ),

                  ),
                  Container(
                    alignment: Alignment.center,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.deepPurpleAccent,
                      ) ,
                      child: Text('mot de passe oublié ?'),
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) =>  const PasswordReset()));
                      },
                    ),
                  ),

                ],
              ),
            ),


          ],
        ),
      ),
    );
  }

  void _togglepassword() {
    setState(() {
      hidepassword=!hidepassword ;
    });
  }
 void signIn(String email, String password) async {
     try {
       await _auth
           .signInWithEmailAndPassword(email: email, password: password)
           .then((uid) => {
         Fluttertoast.showToast(msg: "Login Successful"),
         Navigator.of(context).pushReplacement(
             MaterialPageRoute(builder: (context) => Profile())),
       });
     } on FirebaseAuthException catch (error) {
       switch (error.code) {
         case "invalid-email":
           errorMessage = "Your email address appears to be malformed.";

           break;
         case "wrong-password":
           errorMessage = "Your password is wrong.";
           break;
         case "user-not-found":
           errorMessage = "User with this email doesn't exist.";
           break;
         case "user-disabled":
           errorMessage = "User with this email has been disabled.";
           break;
         case "too-many-requests":
           errorMessage = "Too many requests";
           break;
         case "operation-not-allowed":
           errorMessage = "Signing in with Email and Password is not enabled.";
           break;
         default:
           errorMessage = "An undefined Error happened.";
       }
       Fluttertoast.showToast(msg: errorMessage!);
       print(error.code);
     }

 }
}
