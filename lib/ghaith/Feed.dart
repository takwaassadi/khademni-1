// ignore_for_file: file_names, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:khademni/ghaith/TopBar.dart';
import 'package:khademni/ghaith/Job_offer.dart';
import 'package:khademni/ghaith/job_card.dart';
import 'package:khademni/profile.dart';

class FeedPage extends StatefulWidget {
  const FeedPage({ Key? key }) : super(key: key);

  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<FeedPage> {
  List<JobOffer> persons=[
    JobOffer(
      Company:"InstaDeep",
      img:'img/instadeep.png',
      job:"Data Science student needed",
      clock:"4h per day",
    ),
    JobOffer(
      Company: "Actia",
      img:"img/actia.jpg",
      job:"FullStack Developer needed",
      clock:"3h per day", 
    ),
     JobOffer(
      Company: "Cognira",
      img:"img/cognira.jpg",
      job:"Frontend Developer needed",
      clock:"2h per day", 
    ),
     JobOffer(
      Company: "Telnet",
      img:"img/telnet.png",
      job:"Graphic designer needed",
      clock:"3h per day", 
    ),
     JobOffer(
      Company: "Wevio",
      img:"img/wevio.png",
      job:"Embedded Systems needed",
      clock:"5h per day", 
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [

        Container(

          padding: const EdgeInsets.fromLTRB(18, 50, 18, 18),
          // ignore: prefer_const_constructors
          color:Color(0xFF7779F1),
            child: Row(
              mainAxisAlignment:MainAxisAlignment.center,
              children: [
                Text("Job Seeker Feed ",
                  style:TextStyle(fontWeight: FontWeight.bold,fontSize: 25 ,color: Colors.white),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.white,

                  ) ,
                  child: const Text('Profile',
                    style:TextStyle(fontWeight: FontWeight.normal,fontSize: 20 ,color: Colors.white),),
                  onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) =>  const Profile()));
                  },
                ),
              ],

            ),
        ),
        SizedBox(height: 10,),
        TopBar(),
        SizedBox(height: 10,),
        Expanded(child: SingleChildScrollView(
          child: Column(
            children: persons.map((p) {
              return JobCard(jobOffer:p);
            }).toList()
          ),
        ))

      ],
      
    );
  }
}