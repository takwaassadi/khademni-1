import 'package:flutter/material.dart'; // ignore: file_names
import 'package:khademni/log_in.dart';
import 'package:khademni/sign_up.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    Color c = const Color(0xFF7779F1);
    return  Scaffold(
      backgroundColor: c ,
      body: Column(
        children: [
          Container(
            width: w,
            height: h*0.7,
            decoration: const BoxDecoration(
               image: DecorationImage(
                image: AssetImage(
                  "img/pageloula.PNG"
                ),
                fit: BoxFit.cover
              )
            ),
          ),
          SizedBox(height: 30),
          ElevatedButton(onPressed: (){
            Navigator.push(context,
                MaterialPageRoute(
                builder: (context) =>  const LogInPageFinal()));
          },
            style: ElevatedButton.styleFrom(
              minimumSize: Size(350, 60),
              primary: Colors.white,
              onPrimary: Colors.black,
              shape:  RoundedRectangleBorder(
                borderRadius:  BorderRadius.circular(30.0),
              )

            ),
            child: const Text(
                "LOG IN",
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF7779F1),
                  )
              ),

          ),
          SizedBox(height: 10),
          ElevatedButton(onPressed: (){
            Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) =>  const SignUp()));
          },
            style: ElevatedButton.styleFrom(
                minimumSize: Size(350, 60),
                primary: Colors.white,
                onPrimary: Colors.black,
                shape:  RoundedRectangleBorder(
                  borderRadius:  BorderRadius.circular(30.0),
                )

            ),
            child: const Text(
                "SIGN UP",
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF7779F1)
                )
            ),

          ),
        ]
      )

    );
  }


}


